
select*from person1

select*from place1

select*from visit1


--------------création des tables----------------------
create table IF NOT EXISTS person1 (
person_id INT primary KEY NOT NULL,
person_name VARCHAR(30) NOT NULL,
health_status VARCHAR (20) not null,
confirmed_status_date DATE not null
)

create TABLE IF not EXISTS  place1 (
place_id INT primary key not null,
place_name VARCHAR(30) not null,
place_type VARCHAR (20) not null
)

create table IF NOT EXISTS visit1(
visit_id int primary key not null,
person_id int not null,
place_id int not null,
start_datetime date not null,
end_datetime date not null
)

--insertions des données pour les 3 tables 
--Modification des tables avec TIMESTAMP

--J'ai essayé de modifier les dates avec les lignes ci-dessous mais sans succès( j'ai essayé  d(autres méthodes)
--par conséquent j'ai fait des lignes de codes mais je n'ai pas pu avoir les tableaux à la fin à cause du problème de date que je n'arrive pas à régler 


--pour person1 (il y a des dates)

ALTER TABLE person1 ADD COLUMN news_ts TIMESTAMP;
UPDATE person1 SET new_ts = to_timestamp(confirmed_status_date , 'dd/MM/YYYY HH24:MI')

ALTER TABLE person1 DROP COLUMN confirmed_status_date;
ALTER TABLE person1 RENAME COLUMN new_ts TO confirmed_status_date;

--pour visit (il y a également des dates)

ALTER TABLE visit1 ADD COLUMN new_ts1 TIMESTAMP, ADD COLUMN new_ts2 TIMESTAMP;
UPDATE visit1 SET new_ts1 = to_timestamp(start_datetime, 'dd/MM/YYYY HH24:MI');
UPDATE visit1 SET new_ts2 = to_timestamp(end_datetime, 'dd/MM/YYYY HH24:MI');

sit DROP COLUMN START_datetime, DROP COLUMN end_datetime;
sit RENAME COLUMN new_ts1 TO start_datetime;
sit RENAME COLUMN new_ts2 TO end_datetime;

--question 1

-- Tout d'abord nous allons regarder les lieux qu'a fréquenté Landyn Greer:

SELECT v.place_id, p.place_name
FROM visit1 v INNER JOIN place1 p 
ON v.place_id = p.place_id
WHERE person_id=1

-- lieux 33 et 88 :

select p.person_name, p.health_status 
from person1 p inner join visit1 v
on p.person_id  = v.person_id  
where v.place_id = 33,
tsrange (start_datetime, end_datetime)&&
tsrange '(2020-10-07 20:54:00.000,2020-10-08 03:44:00.000)'
where v.place_id = 88 ,
tsrange (start_datetime, end_datetime) &&
tsrange '(2020-09-26 18:28:00.000,2020-09-26 21:40:00.000)'
AND p.person != 'Landyn Greer'

-- question 2 


select count (*) as health_status, p.place_id
from visit1 v
join place1 p on v.place_id = p.place_id 
join person1 n on v.person_id = n.person_id 
where n.health_status ='Sick', v. start_datetime overlaps v.end_datetime,and p.place_type = 'Bar', n.confirmed_status_date between v.start_datetime and v.end_datetime  
group by p.place_id 

-- J'aurais pu répondre à la question en créant une nouvelle table (jointure des 3 tables)

CREATE TABLE ensemble (
person_id INT NOT NULL,
person_name VARCHAR(30) NOT NULL,
health_status VARCHAR (20) not null,
confirmed_status_date DATE not NULL,
place_name VARCHAR(30) not null,
place_type VARCHAR (20) not NULL,
visit_id int not null,
place_id int not null,
start_datetime date not null,
end_datetime date not null
)
-- Insertion des données dans la nouvelle table avec clic-bouton
-- question 3 
-- Observation des visites de Taylor Luna 

select v.place_id, v.start_datetime, v.end_datetime,p.person_name  
from visit1 v inner join person1 p 
on v.person_id  =p.person_id 
where p.person_name ='Taylor Luna'

select p.person_name
from person1 p inner join visit1 v
on p.person_id  = v.person_id  
where v.place_id = 72, v.start_datetime >= '2020-09-23 05:51', v.end_datetime  <= '2020-09-23 11:05:00.000', 
v.place_id = 8, v.start_datetime >= '2020-09-18 23:37:00.000', v.end_datetime  <= '2020-09-19 05:52:00.000', 
v.place_id = 84, v.start_datetime >= '2020-13-09 20:47:00.000', v.end_datetime  <= '2020-09-14 07:22:00.000',
v.place_id = 26, v.start_datetime >= '2020-09-23 11:27:00.000', v.end_datetime  <= '2020-09-23 20:28:00.000',
v.place_id = 52, v.start_datetime >= '2020-10-09 14:16:00.000', v.end_datetime  <= '2020-10-09 23:06:00.000',
v.place_id = 4, v.start_datetime >= '2020-09-14 15:40:00.000', v.end_datetime  <= '2020-09-15 06:23:00.000',
v.place_id = 90, v.start_datetime >= '2020-10-03 22:19:00.000', v.end_datetime  <= '2020-10-04 07:19:00.000',
v.place_id = 29, v.start_datetime >= '2020-10-09 15:03:00.000', v.end_datetime <= '2020-10-10 01:51:00.000',
v.place_id = 84, v.start_datetime >= '2020-10-09 19:18:00.000', v.end_datetime  <= '2020-10-10 04:52:00.000',
v.place_id = 5, v.start_datetime >= '2020-10-02 04:07:00.000', v.end_datetime  <= '2020-10:02 14:13:00.000'
AND p.person != 'Taylor Luna'

-- question 4

select count (*) as health_status,p.place_name,AVG (v.end_datetime - v.start_datetime) 
from visit1 v
join place1 p  on v.place_id = p.place_id 
join person1 n on n.person_id = v.person_id
where n.health_status = 'Sick'
group by p.place_name 


-- question 5

select count (*) as health_status,p.place_name,AVG (v.end_datetime - v.start_datetime) 
from visit1 v
join place1 p  on v.place_id = p.place_id 
join person1 n on n.person_id = v.person_id
where n.health_status = 'Healthy'
group by p.place_name 



